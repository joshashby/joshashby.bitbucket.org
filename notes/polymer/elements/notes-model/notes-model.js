(function() {
  Polymer({
    ready: function() {
      return this.asyncMethod(function() {
        return this.notes = this.notes || [];
      });
    },
    addNote: function() {
      return this.notes.push({
        title: "Untitled Note",
        hash: chance.hash({
          length: 15
        }),
        tags: [],
        cells: [
          {
            type: 'markdown',
            body: 'markdown cell \\o/ # YES'
          }, {
            type: 'latex',
            body: "I_{S} = \\left( 1\\over2 \\right) \\mu_{n} C_{ox}\n\\left( V_{GS} - V_{TH} \\right)^2\n\\left( 1- \\lambda V_{DS} \\right)"
          }, {
            type: 'code',
            language: 'javascript',
            body: 'a = function(b, c) { return b+c }'
          }
        ]
      });
    },
    removeNote: function(note) {
      return _.remove(this.notes, note);
    },
    save: function() {
      return this.$.storage.save();
    }
  });

}).call(this);

//# sourceMappingURL=notes-model.js.map
