Polymer
  ready: ->
    @asyncMethod ->
      @notes = @notes || []

  addNote: ->
    @notes.push
      title: "Untitled Note"
      hash: chance.hash({length: 15})
      tags: []
      cells: [
        {
          type: 'markdown'
          body: 'markdown cell \\o/ # YES'
        }
        {
          type: 'latex'
          body: """
          I_{S} = \\left( 1\\over2 \\right) \\mu_{n} C_{ox}
          \\left( V_{GS} - V_{TH} \\right)^2
          \\left( 1- \\lambda V_{DS} \\right)
          """
        }
        {
          type: 'code'
          language: 'javascript'
          body: 'a = function(b, c) { return b+c }'
        }
      ]

  removeNote: (note) ->
    _.remove @notes, note

  save: ->
    @$.storage.save()
