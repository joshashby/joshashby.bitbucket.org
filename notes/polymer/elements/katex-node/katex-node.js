(function() {
  Polymer({
    text: '',
    textChanged: function(o, n) {
      if (n) {
        return this.$.renderResult.innerHTML = katex.renderToString(this.text);
      }
    }
  });

}).call(this);

//# sourceMappingURL=katex-node.js.map
