(function() {
  Polymer({
    text: '',
    textChanged: function(o, n) {
      var language;
      if (n) {
        language = this.language || 'javascript';
        return this.$.renderResult.innerHTML = hljs.highlight(language, this.text).value;
      }
    }
  });

}).call(this);

//# sourceMappingURL=highlight-node.js.map
