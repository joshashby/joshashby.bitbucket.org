Polymer
  text: ''

  textChanged: (o, n) ->
    if n
      language = @language || 'javascript'
      @$.renderResult.innerHTML = hljs.highlight(language, @text).value
