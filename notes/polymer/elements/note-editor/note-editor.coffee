Polymer
  currentCellIndex: null

  ready: ->

  currentCellIndexChanged: ->
    @fire 'changedCell',
      cell: @currentCellIndex

  addCell: ->
    @fire 'addCell'
