(function() {
  Polymer({
    currentCellIndex: null,
    ready: function() {},
    currentCellIndexChanged: function() {
      return this.fire('changedCell', {
        cell: this.currentCellIndex
      });
    },
    addCell: function() {
      return this.fire('addCell');
    }
  });

}).call(this);

//# sourceMappingURL=note-editor.js.map
