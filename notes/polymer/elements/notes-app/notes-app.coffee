Polymer
  currentNote: null
  currentCell: null
  languages: hljs.listLanguages()

  toUpperCase: (value) ->
    value.toUpperCase()

  saving: false

  ready: ->
    @addEventListener 'changedCell', (e) =>
      @currentCell = @currentNote.cells[e.detail.cell]

    @addEventListener 'addCell', (e) =>
      @addCell()

    @asyncMethod ->
      @updateRouteData()

    setInterval =>
      @save()
    , 15000

  updateRouteData: ->
    @currentNote = _.find @notes, hash: @route

  observe:
    route: 'updateRouteData'

  addNote: ->
    @$.storage.addNote()

  addCell: ->
    @currentNote.cells.push
      type: 'markdown'
      body: ''

    @currentCell = @currentNote.cells.length

  removeNote: (e, d, t) ->
    note = _.find @notes, hash: t.attributes['hash'].value
    @$.storage.removeNote note

  removeCell: ->
    _.remove @currentNote.cells, @currentCell

  save: ->
    @saving = true
    @$.storage.save()
    setTimeout =>
      @saving = false
    , 1000
