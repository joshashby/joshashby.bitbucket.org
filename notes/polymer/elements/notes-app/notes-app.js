(function() {
  Polymer({
    currentNote: null,
    currentCell: null,
    languages: hljs.listLanguages(),
    toUpperCase: function(value) {
      return value.toUpperCase();
    },
    saving: false,
    ready: function() {
      this.addEventListener('changedCell', (function(_this) {
        return function(e) {
          return _this.currentCell = _this.currentNote.cells[e.detail.cell];
        };
      })(this));
      this.addEventListener('addCell', (function(_this) {
        return function(e) {
          return _this.addCell();
        };
      })(this));
      this.asyncMethod(function() {
        return this.updateRouteData();
      });
      return setInterval((function(_this) {
        return function() {
          return _this.save();
        };
      })(this), 15000);
    },
    updateRouteData: function() {
      return this.currentNote = _.find(this.notes, {
        hash: this.route
      });
    },
    observe: {
      route: 'updateRouteData'
    },
    addNote: function() {
      return this.$.storage.addNote();
    },
    addCell: function() {
      this.currentNote.cells.push({
        type: 'markdown',
        body: ''
      });
      return this.currentCell = this.currentNote.cells.length;
    },
    removeNote: function(e, d, t) {
      var note;
      note = _.find(this.notes, {
        hash: t.attributes['hash'].value
      });
      return this.$.storage.removeNote(note);
    },
    removeCell: function() {
      return _.remove(this.currentNote.cells, this.currentCell);
    },
    save: function() {
      this.saving = true;
      this.$.storage.save();
      return setTimeout((function(_this) {
        return function() {
          return _this.saving = false;
        };
      })(this), 1000);
    }
  });

}).call(this);

//# sourceMappingURL=notes-app.js.map
