"use strict"

planet = """
                                                     ___
                                                  ,o88888
                                               ,o8888888'
                         ,:o:o:oooo.        ,8O88Pd8888"
                     ,.::.::o:ooooOoOoO. ,oO8O8Pd888'"
                   ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O"
                  , ..:.::o:ooOoOOOO8OOOOo.FdO8O8"
                 , ..:.::o:ooOoOO8O888O8O,COCOO"
                , . ..:.::o:ooOoOOOO8OOOOCOCO"
                 . ..:.::o:ooOoOoOO8O8OCCCC"o
                    . ..:.::o:ooooOoCoCCC"o:o
                    . ..:.::o:o:,cooooCo"oo:o:
                 `   . . ..:.:cocoooo"'o:o:::'
                 .`   . ..::ccccoc"'o:o:o:::'
                :.:.    ,c:cccc"':.:.:.:.:.'
              ..:.:"'`::::c:"'..:.:.:.:.:.'
            ...:.'.:.::::"'    . . . . .'
           .. . ....:."' `   .  . . ''
         . . . ...."'
         .. . ."'     -hrr-
        .


"""

warning = """
Sassy Ploymer Coffee is ready to rock!

This is *HIGHLY* broken and mostly experimental. If you're reading this,
hopefully you know how to fix things or at least avoid the broken stuff. Feel
free to poke around and see what you can improve.

- transientBug
"""

((document) ->
  document.addEventListener "polymer-ready", ->
    console.log planet, warning

# wrap document so it plays nice with other libraries
# http://www.polymer-project.org/platform/shadow-dom.html#wrappers
) wrap(document)
